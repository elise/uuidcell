use std::{hash::Hash, ops::{Deref, DerefMut}};

use rand::{RngCore, thread_rng};

use crate::UuidCell;

impl<T> UuidCell<T> {
    /// Instanciates a new UuidCell
    pub fn new(inner: T) -> Self {
        let mut rng = thread_rng();
        Self {
            uuid: (rng.next_u64() as u128) << 64 | (rng.next_u64() as u128),
            inner,
        }
    }
}

impl<T> Hash for UuidCell<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.uuid.hash(state)
    }
}

impl<T> Deref for UuidCell<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}


impl<T> DerefMut for UuidCell<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}