//! `UuidCell` for `Hash`, supports both no_std (pRNG via `oorandom`) and std (slightly better pRNG via `rand`). Enable either `no_std` or `std` feature.
#![deny(missing_docs)]
#![cfg_attr(
    feature = "no_std",
    no_std
)]

#[cfg(any(all(feature = "no_std", feature = "std"), all(not(feature = "no_std"), not(feature = "std"))))]
compile_error!("Please enable one, and only one, of `no_std` or `std`");

#[cfg_attr(feature = "no_std", path = "no_std.rs")]
#[cfg_attr(feature = "std", path = "std.rs")]
#[cfg_attr(all(not(feature = "no_std"), not(feature = "std")), path = "no_impl.rs")]
mod imp;

/// Cell with a Uuid
pub struct UuidCell<T> {
    uuid: u128,
    inner: T,
}

pub use imp::*;