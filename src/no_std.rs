use core::{hash::Hash, ops::{Deref, DerefMut}};

use oorandom::Rand64;

use crate::UuidCell;

/// `no_std` generator for a `UuidCell`
pub struct UuidCellBuilder {
    rand: Rand64,
}

impl UuidCellBuilder {
    /// Returns a new `UuidCellBuilder` with the seed `6669420987651428059`
    pub fn new() -> Self {
        Self::seed(6669420987651428059)
    }

    /// Returns a new `UuidCellBuilder` with a custom seed
    pub fn seed(seed: u128) -> Self {
        Self {
            rand: Rand64::new(seed)
        }
    }

    /// Builds a new `UuidCell<T>`
    pub fn build<T>(&mut self, inner: T) -> UuidCell<T> {
        UuidCell {
            uuid: (self.rand.rand_u64() as u128) << 64 | self.rand.rand_u64() as u128,
            inner,
        }
    }
}

impl<T> Hash for UuidCell<T> {
    fn hash<H: core::hash::Hasher>(&self, state: &mut H) {
        self.uuid.hash(state)
    }
}

impl Default for UuidCellBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> Deref for UuidCell<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}


impl<T> DerefMut for UuidCell<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}