# UuidCell

`UuidCell` for `Hash`, supports both no_std (pRNG via `oorandom`) and std (slightly better pRNG via `rand`).

## Usage

You need to enable one of the following features:

* `no_std`
* `std`

### `no_std`

#### Builder

With `no_std` you need to keep a mutable `UuidCellBuilder` around. You can call `UuidCellBuilder::new()` to obtain one, `UuidCellBuilder::seed(some u128)` to obtain one with a custom seed, then you can call `UuidCellBuilder::build::<T>::(inner)` to obtain a new `UuidCell`

### `std`

With `std` you can just call `UuidCell::new(inner)`